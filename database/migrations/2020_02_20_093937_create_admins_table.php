<?php

use App\Models\Admin;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Admin::getTableName())) {
            Schema::create(Admin::getTableName(), static function(Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('login')->unique();
                $table->string('password');
                $table->string('remember_token')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(sprintf('DROP TABLE IF EXISTS %s CASCADE', Admin::getTableName()));
    }
}
