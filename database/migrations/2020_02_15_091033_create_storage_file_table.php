<?php

use App\Models\File\StorageFile;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorageFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(StorageFile::getTableName())) {
            Schema::create(StorageFile::getTableName(), static function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('path')->index();
                $table->string('name');
                $table->string('hash')->index();
                $table->timestamps();

                $table->unique(['path', 'name']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(sprintf('DROP TABLE IF EXISTS %s CASCADE', StorageFile::getTableName()));
    }
}
