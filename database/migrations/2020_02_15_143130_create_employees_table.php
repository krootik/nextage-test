<?php

use App\Models\Employee;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Employee::getTableName())) {
            Schema::create(Employee::getTableName(), static function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->date('employment_date');
                $table->float('salary');
                $table->unsignedBigInteger('avatar_id')->nullable();
                $table->unsignedBigInteger('position_id')->nullable()->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(sprintf('DROP TABLE IF EXISTS %s CASCADE', Employee::getTableName()));
    }
}
