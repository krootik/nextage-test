<?php

use App\Models\Position;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Position::getTableName())) {
            Schema::create(Position::getTableName(), static function(Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name')->unique();
                $table->unsignedBigInteger('boss_id')->nullable()->index();
                $table->unsignedBigInteger('parent_position_id')->nullable()->index();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(sprintf('DROP TABLE IF EXISTS %s CASCADE', Position::getTableName()));
    }
}
