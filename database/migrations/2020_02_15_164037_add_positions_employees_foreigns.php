<?php

use App\Models\Employee;
use App\Models\File\StorageFile;
use App\Models\Position;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionsEmployeesForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Position::getTableName(), static function(Blueprint $table) {
            $table->foreign('boss_id', 'pos_boss_rel')
                  ->references('id')
                  ->on(Employee::getTableName())
                  ->onDelete('RESTRICT')
                  ->onUpdate('CASCADE');
            $table->foreign('parent_position_id', 'pos_parent_rel')
                  ->references('id')
                  ->on(Position::getTableName())
                  ->onDelete('RESTRICT')
                  ->onUpdate('RESTRICT');
        });
        Schema::table(Employee::getTableName(), static function (Blueprint $table) {
            $table->foreign('avatar_id', 'emp_avatar_rel')
                  ->references('id')
                  ->on(StorageFile::getTableName())
                  ->onDelete('SET NULL')
                  ->onUpdate('CASCADE');
            $table->foreign('position_id', 'emp_pos_rel')
                  ->references('id')
                  ->on(Position::getTableName())
                  ->onDelete('RESTRICT')
                  ->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Employee::getTableName(), static function (Blueprint $table) {
            $table->dropForeign('emp_pos_rel');
            $table->dropForeign('emp_avatar_rel');
        });
        Schema::table(Position::getTableName(), static function (Blueprint $table) {
            $table->dropForeign('pos_parent_rel');
            $table->dropForeign('pos_boss_rel');
        });
    }
}
