<?php

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Database\Seeder;

class CreateEmployeesSeeder extends Seeder
{
    public $employeesPerPosition = 10000;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empCount = max(0, $this->employeesPerPosition - 1);
        $positions = Position::all();

        foreach ($positions as $positon) {
            $posId = $positon->getKey();

            $positon->boss_id = factory(Employee::class, 'without_position')->create(['position_id' => $posId])->getKey();
            $positon->save();

            if ($empCount) {
                factory(Employee::class, 'without_position', $empCount)->create(['position_id' => $posId]);
            }
        }
    }
}
