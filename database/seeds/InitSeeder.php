<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateAdminSeeder::class);
        $this->call(CreatePositionsSeeder::class);
        $this->call(CreateEmployeesSeeder::class);
    }
}
