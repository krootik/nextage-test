<?php

use App\Models\Position;
use Illuminate\Database\Seeder;

class CreatePositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Position::class, 1)->create()->each(static function(Position $pos1) {
            factory(Position::class, 1)->create(['parent_position_id' => $pos1])->each(static function(Position $pos2) {
                factory(Position::class, 1)->create(['parent_position_id' => $pos2])->each(static function(Position $pos3) {
                    factory(Position::class, 1)->create(['parent_position_id' => $pos3])->each(static function(Position $pos4) {
                        factory(Position::class, 1)->create(['parent_position_id' => $pos4]);
                    });
                });
            });
        });
    }
}
