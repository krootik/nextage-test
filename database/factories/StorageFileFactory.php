<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin;
use App\Models\File\StorageFile;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(StorageFile::class, static function (Faker $faker) {
    $fs = resolve(Filesystem::class);

    $storagePath = Storage::path(StorageFile::UPLOADS_PATH);
    $imgName = $faker->image($storagePath);
//    Storage::putFileAs($storageFile->path, $tmpFile, $storageFile->name);
    $imgPath = $storagePath . $imgName;

    return [
        'path' => StorageFile::UPLOADS_PATH,
        'name' => StorageFile::uniqueName($fs->extension($imgPath)),
        'hash' => $fs->hash($imgPath),
    ];
});
