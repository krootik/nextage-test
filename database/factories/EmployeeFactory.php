<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Employee;
use App\Models\File\StorageFile;
use App\Models\Position;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Employee::class, static function (Faker $faker) {
    return [
        'name' => $faker->name,
        'employment_date' => $faker->dateTimeInInterval('-20 months', '-3 months'),
        'salary' => $faker->randomFloat(2, 500, 5000),
        'position_id' => Position::all()->random()->getKey(),
//        'avatar_id' => factory(StorageFile::class)->create()->getKey()
    ];
});

$factory->define(Employee::class, static function (Faker $faker) {
    return [
        'name' => $faker->name,
        'employment_date' => $faker->dateTimeInInterval('-20 months', '-3 months'),
        'salary' => $faker->randomFloat(2, 500, 5000),
//        'avatar_id' => factory(StorageFile::class)->create()->getKey()
    ];
}, 'without_position');
