<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@doLogin')->name('login-action');

Route::group([
    'middleware' => 'auth'
], static function() {
    Route::any('logout', 'AuthController@doLogout')->name('logout-action');

    Route::get('/', 'EmployeesContoller@index')->name('index');
});
