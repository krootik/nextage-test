<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'employee',
    'as' => 'employee-'
], static function() {

    Route::get('/list', 'EmployeesContoller@getEmployees')->name('list');
    Route::get('/{id}', 'EmployeesContoller@getEmployee')->where('id', '[0-9]+')->name('single');

    Route::post('/', 'EmployeesContoller@createEmployee')->name('create');
    Route::post('/{id}', 'EmployeesContoller@updateEmployee')->where('id', '[0-9]+')->name('update');
    Route::delete('/{id}', 'EmployeesContoller@deleteEmployee')->where('id', '[0-9]+')->name('delete');
});

Route::group([
    'prefix' => 'position',
    'as' => 'position-'
], static function() {

    Route::get('/list', 'EmployeesContoller@getPositions')->name('list');
    Route::get('/{id}', 'EmployeesContoller@getPosition')->where('id', '[0-9]+')->name('single');

    Route::post('/', 'EmployeesContoller@createPosition')->name('create');
    Route::post('/{id}', 'EmployeesContoller@updatePosition')->where('id', '[0-9]+')->name('update');
    Route::delete('/{id}', 'EmployeesContoller@deletePosition')->where('id', '[0-9]+')->name('delete');
});
