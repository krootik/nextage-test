if(window.FileReader) {
    $(document).ready(function () {
        $(document).on('change', 'input[type="file"][accept^="image"]:not([multiple])', function() {
            let fr = new FileReader(),
                imgNode = findImgPreviewContainer($(this));

            if (imgNode) {
                fr.onloadend = function () {
                    imgNode.attr('src', fr.result)
                };
                fr.readAsDataURL($(this)[0].files[0]);
            }

        })
    });

    function findImgPreviewContainer(node, maxDepth = 4) {
        if (maxDepth === 0) {
            return null;
        }
        let imgNode = $(node).find('img');
        if (imgNode.length) {
            return imgNode;
        }
        return findImgPreviewContainer(node.parent(), maxDepth - 1);
    }
}