$(document).ready( function () {
    $('#employees_list').DataTable({
        lengthMenu: [ 10, 50, 100, 1000 ],
        dom: 'ftlpi',
        columns: [
            {title: 'ID', data:'id', name:'id'},
            {title: 'ФИО', data:'name', name:'name'},
            {title: 'Фото', data:(row) => row.ava_url ? '<img height="100" src="' + row.ava_url + '">' : 'Нет фото', orderable: false},
            {title: 'Должность', data:(row) => row.position_name ? row.position_name : 'Должность не назначена', name:'position'},
            {title: 'Дата приёма', data:'works_from', name:'employment_date'},
            {title: 'З/п', data:'salary', name:'salary'},
        ],
        serverSide: true,
        searchDelay: 500,
        ajax: {
            url: URLS.list,
            data: function(params) {
                params.search = params.search.value;
                let sortParams = {};
                if (typeof params.order === 'object') {
                    $.each(params.order, (i,sort) => {
                        sortParams[params.columns[sort.column].name] = sort.dir;
                    });
                    delete params.order;
                }
                params.sort = sortParams;
                delete params.columns;
            },
            beforeSend: function() {
                let previousRequest = $('#employees_list').DataTable().settings()[0].jqXHR;
                if (previousRequest && previousRequest.readyState != 4) {
                    previousRequest.abort();
                }
            }
        },
    });
} );