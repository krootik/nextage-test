let employeesFilters = {
    position_id: 0
};
let allPositions = [];
let allEmployees = [];
$(document).ready( function () {
    init();
    fillPositionsList();
    fillEmployeesList();

    $(document).on('click', '.view_position_employee', function() {
        employeesFilters.position_id = parseInt($(this).parents('tr').data('id'));
        $('#employees_list').DataTable().ajax.reload();
    });

    $(document).on('click', '.create_position', function() {
        let modal = $('#create_position_modal'),
            dataRow = $(this).parents('tr');
        if (dataRow && dataRow.length) {
            refreshSelectPicker($('[name="parent_position_id"]', modal).val(dataRow.data('id')))
        }
        modal.modal('show');
    });
    $('#create_position_form').submit(function (e) {
        e.preventDefault();

        let formData = new FormData(this);
        if (!parseInt(formData.get('parent_position_id'))) {
            formData.delete('parent_position_id')
        }

        $.ajax({
            url: URLS.position_create,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                if (!response.errors) {
                    fillPositionsList(true);
                    let parentNodeId = parseInt(response.parent_position_id),
                        openedParentNode = $('.treegrid-' + parentNodeId);

                    if (openedParentNode && openedParentNode.length ) {
                        let bossId = parseInt(response.boss_id);
                        openedParentNode
                            .treegrid('add', [
                                $(document.createElement('tr'))
                                    .addClass('treegrid-' + response.id)
                                    .addClass('treegrid-parent-' + parentNodeId)
                                    .attr({
                                        'data-count': 1,
                                        'data-id': response.id,
                                        'data-parent_id': parentNodeId,
                                        'data-boss_id': bossId,
                                    })
                                    .append([
                                        $('<td/>').text(response.name),
                                        $('<td/>').text(bossId ? 'Ожидайте...' : 'Нет начальника'),
                                        $('<td/>').append(
                                            $('<div/>')
                                                .addClass('btn-group btn-group-xs')
                                                .append([
                                                    $(document.createElement('div'))
                                                        .addClass('btn btn-success create_employee')
                                                        .html('<i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-user"></i>'),
                                                    $(document.createElement('div'))
                                                        .addClass('btn btn-info view_position_employee')
                                                        .html('<i class="glyphicon glyphicon-eye-open"></i><i class="glyphicon glyphicon-user"></i>'),
                                                ])
                                        ),
                                        $('<td/>').append(
                                            $('<div/>')
                                                .addClass('btn-group btn-group-xs')
                                                .append([
                                                    $(document.createElement('div'))
                                                        .addClass('btn btn-success create_position')
                                                        .html('<i class="glyphicon glyphicon-plus"></i>'),
                                                    $(document.createElement('div'))
                                                        .addClass('btn btn-warning edit_position')
                                                        .html('<i class="glyphicon glyphicon-edit"></i>'),
                                                    $(document.createElement('div'))
                                                        .addClass('btn btn-danger delete_position')
                                                        .html('<i class="glyphicon glyphicon-trash"></i>'),
                                                ])
                                        ),
                                    ])[0].outerHTML
                            ]);

                        if (bossId) {
                            $.get(URLS.employee_get.replace(/\d+$/,  bossId), {id: bossId}, function(responseBoss) {
                                $('td:nth-child(2)', $('.treegrid-' + response.id)).text(responseBoss.name);
                            });
                        }
                    }

                    $('#create_position_modal').modal('hide');
                } else {
                    alert('Data not saved');
                }
            }
        })
    });

    $(document).on('click', '.edit_position', function() {
        let modal = $('#edit_position_modal'),
            dataRow = $(this).parents('tr'),
            empId = dataRow.data('id');
        $.get(URLS.position_get.replace(/\d+$/,  empId), {id: empId}, function(response) {
            $('[name="name"]', modal).val(response.name);
            refreshSelectPicker($('[name="parent_position_id"]', modal).val(response.parent_position_id || 0));
            refreshSelectPicker($('[name="boss_id"]', modal).val(response.boss_id));
            modal.data('id', empId);
            modal.modal('show');
        });
    });
    $('#edit_position_form').submit(function (e) {
        e.preventDefault();

        let editId = $('#edit_position_modal').data('id'),
            formData = new FormData(this);
        if (!parseInt(formData.get('parent_position_id'))) {
            formData.delete('parent_position_id')
        }
        formData.set('id', editId);

        $.ajax({
            url: URLS.position_update.replace(/\d+$/, editId),
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                if (!response.errors) {
                    fillPositionsList(true);
                    let gridNode = $('.treegrid-' + editId);

                    let parentId = parseInt(response.parent_position_id),
                        bossId = parseInt(response.boss_id);
                    if (gridNode && gridNode.length) {
                        if (parseInt(gridNode.data('parent_id')) != parentId) {
                            let parentNode = $('.treegrid-' + parentId);
                            if (parentNode && parentNode.length) {
                                gridNode.treegrid('move', parentNode, 1);
                            } else {
                                gridNode.treegrid('remove')
                            }
                        }
                        if (parseInt(gridNode.data('boss_id')) != bossId) {
                            $.get(URLS.employee_get.replace(/\d+$/,  bossId), {id: bossId}, function(responseBoss) {
                                $('td:nth-child(2)', gridNode).text(responseBoss.name);
                            });
                        }
                        $('td:first div', gridNode).text(response.name);
                        gridNode.data('parent_id', parentId);
                        gridNode.data('boss_id', bossId);
                    }

                    $('#edit_position_modal').modal('hide');
                } else {
                    alert('Data not saved');
                }
            }
        })
    });

    $(document).on('click', '.delete_position', function() {
        if (confirm('Удалить должность?')) {
            let dataRow = $(this).parents('tr'),
                deleteId = dataRow.data('id');
            $.ajax({
                url: URLS.position_delete.replace(/\d+$/, deleteId),
                type: 'DELETE',
                data: {
                    id: deleteId
                },
                success: function(response) {
                    if (!response.errors) {
                        fillPositionsList(true);
                        let gridNode = $('.treegrid-' + deleteId);
                        if (gridNode && gridNode.length) {
                            let parentNodeClass = 'treegrid-parent-' + deleteId,
                                parentNodes = $('.' + parentNodeClass);

                            if (parentNodes && parentNodes.length) {
                                $(parentNodes, function(i, el) {
                                    $(el).treegrid('move', $('.treegrid-' + dataRow.data('parent_id')), 1);
                                });
                            }
                            gridNode.treegrid('remove')
                        }
                    } else {
                        alert('Data not deleted');
                    }
                }
            })
        }
    });

    $(document).on('click', '.create_employee', function() {
        let modal = $('#create_employee_modal'),
            dataRow = $(this).parents('tr');
        if (dataRow && dataRow.length) {
            refreshSelectPicker($('[name="position_id"]', modal).val(dataRow.data('id')))
        }
        modal.modal('show');
    });
    $('#create_employee_form').submit(function (e) {
        e.preventDefault();

        let formData = new FormData(this);

        $.ajax({
            url: URLS.employee_create,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                if (!response.errors) {
                    fillEmployeesList(true);
                    if (employeesFilters.position_id === parseInt(response.position_id)) {
                        $('#employees_list').DataTable().ajax.reload();
                    }
                    $('#create_employee_modal').modal('hide');
                } else {
                    alert('Data not saved');
                }
            }
        })
    });

    $(document).on('click', '.edit_employee', function() {
        let modal = $('#edit_employee_modal'),
            dataRow = $(this).parents('tr'),
            empId = dataRow.data('id');
        $.get(URLS.employee_get.replace(/\d+$/,  empId), {id: empId}, function(response) {
            $('[name="avatar"]', modal).parent().find('img').attr('src', response.ava_url);
            $('[name="name"]', modal).val(response.name);
            $('[name="employment_date"]', modal).val(response.works_from);
            $('[name="salary"]', modal).val(response.salary);

            let posEl = $('[name="position_id"]', modal);
            // posEl.prop('disabled', false);
            // if (empId === parseInt($('treegrid-' + response.position_id).data('boss_id'))) {
            //     posEl.prop('disabled', true)
            // }
            refreshSelectPicker(posEl.val(response.position_id || 0));

            modal.data('id', empId);
            modal.modal('show');
        });
    });
    $('#edit_employee_form').submit(function (e) {
        e.preventDefault();

        let editId = $('#edit_employee_modal').data('id'),
            formData = new FormData(this);
        formData.set('id', editId);

        $.ajax({
            url: URLS.employee_update.replace(/\d+$/, editId),
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                if (!response.errors) {
                    fillEmployeesList(true);
                    if (employeesFilters.position_id === parseInt(response.position_id)) {
                        $('#employees_list').DataTable().ajax.reload();
                    }
                    $('#edit_employee_modal').modal('hide');
                } else {
                    alert('Data not saved');
                }
            }
        })
    });

    $(document).on('click', '.delete_employee', function() {
        if (confirm('Удалить сотрудника?')) {
            let dataRow = $(this).parents('tr'),
                deleteId = dataRow.data('id');
            $.ajax({
                url: URLS.employee_delete.replace(/\d+$/, deleteId),
                type: 'DELETE',
                data: {
                    id: deleteId
                },
                success: function(response) {
                    if (!response.errors) {
                        fillEmployeesList(true);
                        if (employeesFilters.position_id === parseInt(response.position_id)) {
                            $('#employees_list').DataTable().ajax.reload();
                        }
                    } else {
                        alert('Data not deleted');
                    }
                }
            })
        }
    });
} );

function init() {
    $('#employees_positions_tree').treegrid({
        source: function(id, responseCallback) {
            let currentNode = this;
            $.get(URLS.positions_list, {length: -1, parent_position_id: id}, function(response) {
                let nodes = [];
                if (!response.positions.length) {
                    $(currentNode).removeAttr('data-count')
                } else {
                    $(currentNode).attr('data-count', response.positions.length)
                }
                $.each(response.positions, function(i, positionObject) {
                    nodes.push(
                        $(document.createElement('tr'))
                            .addClass('treegrid-' + positionObject.id)
                            .addClass('treegrid-parent-' + currentNode.data('id'))
                            .attr({
                                'data-count': 1,
                                'data-id': positionObject.id,
                                'data-parent_id': currentNode.data('id'),
                                'data-boss_id': positionObject.boss_id,
                            })
                            .append([
                                $('<td/>').text(positionObject.name),
                                $('<td/>').text(positionObject.boss ? positionObject.boss.name : 'Нет начальника'),
                                $('<td/>').append(
                                    $('<div/>')
                                        .addClass('btn-group btn-group-xs')
                                        .append([
                                            $(document.createElement('div'))
                                                .addClass('btn btn-success create_employee')
                                                .html('<i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-user"></i>'),
                                            $(document.createElement('div'))
                                                .addClass('btn btn-info view_position_employee')
                                                .html('<i class="glyphicon glyphicon-eye-open"></i><i class="glyphicon glyphicon-user"></i>'),
                                        ])
                                ),
                                $('<td/>').append(
                                    $('<div/>')
                                        .addClass('btn-group btn-group-xs')
                                        .append([
                                            $(document.createElement('div'))
                                                .addClass('btn btn-success create_position')
                                                .html('<i class="glyphicon glyphicon-plus"></i>'),
                                            $(document.createElement('div'))
                                                .addClass('btn btn-warning edit_position')
                                                .html('<i class="glyphicon glyphicon-edit"></i>'),
                                            $(document.createElement('div'))
                                                .addClass('btn btn-danger delete_position')
                                                .html('<i class="glyphicon glyphicon-trash"></i>'),
                                        ])
                                ),
                            ])
                    );
                });
                responseCallback(nodes);
            });
        },
        // enableMove: true,
        // onMoveOver: function(item, helper, target, position) {
        //     if (target.hasClass('treegrid-8')) return false;
        //     return true;
        // }
    });
    $('#employees_list').DataTable({
        lengthMenu: [ 10, 50, 100, 1000 ],
        dom: 'ftlpi',
        columns: [
            {title: 'ID', data:'id', name:'id'},
            {title: 'ФИО', data:'name', name:'name'},
            {title: 'Фото', data:(row) => row.ava_url ? '<img height="100" src="' + row.ava_url + '">' : 'Нет фото', orderable: false},
            {title: 'Должность', data:(row) => row.position_name ? row.position_name : 'Должность не назначена', name:'position'},
            {title: 'Дата приёма', data:'works_from', name:'employment_date'},
            {title: 'З/п', data:'salary', name:'salary'},
            {title: 'Действия', data:() => '', orderable: false},
        ],
        serverSide: true,
        searchDelay: 500,
        ajax: {
            url: URLS.employees_list,
            data: function(params) {
                $.extend(params, employeesFilters);
                params.search = params.search.value;
                let sortParams = {};
                if (typeof params.order === 'object') {
                    $.each(params.order, (i,sort) => {
                        sortParams[params.columns[sort.column].name] = sort.dir;
                    });
                    delete params.order;
                }
                params.sort = sortParams;
                delete params.columns;
            },
            beforeSend: function() {
                if (!Object.keys(employeesFilters).length) {
                    return false;
                }
                let previousRequest = $('#employees_list').DataTable().settings()[0].jqXHR;
                if (previousRequest && previousRequest.readyState != 4) {
                    previousRequest.abort();
                }
            }
        },
        createdRow: function(tr, rowData) {
            $(tr).attr('data-id', rowData.id);
            $('td:last', $(tr))
                .append(
                    $('<div/>')
                        .addClass('btn-group btn-group-xs')
                        .append(function() {
                            let treeNode = $('.treegrid-' + employeesFilters.position_id),
                                isBoss = rowData.id === parseInt($(treeNode).data('boss_id')),
                                btns = [
                                    $(document.createElement('div'))
                                        .addClass('btn btn-warning edit_employee')
                                        .html('<i class="glyphicon glyphicon-edit"></i>')
                                ];
                            if (!isBoss) {
                                btns.push(
                                    $(document.createElement('div'))
                                        .addClass('btn btn-danger delete_employee')
                                        .html('<i class="glyphicon glyphicon-trash"></i>')
                                )
                            }

                            return btns;
                        })
                )
        }
    });
}

function fillPositionsList(reload) {
    if (!Object.keys(allPositions).length || reload) {
        $.get(URLS.positions_list, {length: -1}, function(response) {
            allPositions = $.extend([], response.positions);
            $('select[name="position_id"], select[name="parent_position_id"]').each(function (i, el) {
                let withEmptyChoice = $(el).attr('name') === 'parent_position_id',
                    positions = $.extend([], response.positions);
                if (withEmptyChoice) {
                    positions.unshift({
                        id: 0,
                        name: 'Нет родителя'
                    })
                }
                $(el).empty();
                fillSelectPicker($(el), positions);
            })
        })
    } else {
        $('select[name="position_id"], select[name="parent_position_id"]').each(function (i, el) {
            let withEmptyChoice = $(el).attr('name') === 'parent_position_id',
                positions = $.extend([], allPositions);
            if (withEmptyChoice) {
                positions.unshift({
                    id: 0,
                    name: 'Нет родителя'
                })
            }

            $(el).empty();
            fillSelectPicker($(el), positions);
        })
    }
}

function fillEmployeesList() {
    if (!Object.keys(allEmployees).length || reload) {
        $.get(URLS.employees_list, {length: -1}, function(response) {
            allEmployees = $.extend([], response.data);
            $('select[name="boss_id"]').each(function (i, el) {
                let employees = response.data;
                $(el).empty();
                fillSelectPicker($(el), employees);
            })
        })
    } else {
        $('select[name="boss_id"]').each(function (i, el) {
            let employees = $.extend([], allEmployees);
            $(el).empty();
            fillSelectPicker($(el), employees);
        })
    }
}

function fillSelectPicker(element, data) {
    $(element)
        .append(function() {
            return $.map(data, function(position) {
                return $('<option/>').attr('value', position.id).text(position.name)
            })
        });
    refreshSelectPicker(element)
}

function refreshSelectPicker(element) {
    $(element).selectpicker('refresh')
}