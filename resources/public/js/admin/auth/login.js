$(document).ready(function () {
    $('#show_hide_pass').one('click', showPass);
});

function showPass() {
    $('[name="password"]', $(this).parents('form')).attr('type', 'text');
    $(this).one("click", hidePass).find('.glyphicon').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
}

function hidePass() {
    $('[name="password"]', $(this).parents('form')).attr('type', 'password');
    $(this).one("click", showPass).find('.glyphicon').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
}