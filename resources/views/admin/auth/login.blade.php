@extends('admin.base-layout')

@section('content')
    <div class="panel panel-info"  id="login_form">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Продолжить как админ</h3>
        </div>
        <form class="panel-body" action="{{ $urls['doLogin'] }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Логин" required="required" name="login">
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Пароль" required="required" name="password">
                    <div class="input-group-addon" id="show_hide_pass">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Войти</button>
            </div>
            <div class="clearfix text-center">
                <label class="checkbox-inline"><input type="checkbox" name="remember"> Запомнить меня</label>
            </div>
        </form>
    </div>
@endsection

@push('styles')
    @css('admin/auth/login')
@endpush

@push('scripts')
    @js('admin/auth/login')
@endpush