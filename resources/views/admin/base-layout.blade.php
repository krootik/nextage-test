@extends('base-layout')

@section('main')
    <main class="container">
        @yield('content')
    </main>
@endsection

@section('header')
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    Nextpage test
                </a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="disabled">
                        <a href="#">
                            Админка
                        </a>
                    </li>
                    @if(isset($title) && !empty($title))
                        @php
                            $urlCurrent = null;
                            if (isset($urls['current']) && !empty($urls['current'])) {
                                $urlCurrent = $urls['current'];
                            }
                        @endphp
                        <li class="@if(!$urlCurrent) disabled @endif">
                            <a href="{{ $urlCurrent ?: '#' }}">
                                {{ $title }}
                            </a>
                        </li>
                    @endif

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        @guest
                            <a href="{{ $urls['login'] }}">
                                Войти
                            </a>
                        @elseauth
                            <a href="{{ $urls['doLogout'] }}">
                                Выход
                            </a>
                        @endif

                    </li>
                    <li>
                        <a href="{{ $urls['front'] }}">
                            Перейти к публичной части
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endsection

@section('footer')
    <p class="text-muted">Footer</p>
@endsection

@push('styles')
    @css('admin/index')
@endpush

@push('scripts')
    @js('admin/index')
@endpush