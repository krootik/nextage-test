@extends('admin.base-layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right btn btn-primary create_position">
                <i class="glyphicon glyphicon-plus"></i>
                Создать должность
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <table id="employees_positions_tree" class="table table-condensed">
                <thead>
                <tr>
                    <th>Название должности</th>
                    <th>Начальник</th>
                    <th>Сотрудники</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data['positions'] as $i => $position)
                        <tr class="treegrid-{{ $position->id }}" data-count="1" data-id="{{ $position->id }}" data-parent_id="" data-boss_id="{{ $position->boss_id }}">
                            <td>{{ $position->name }}</td>
                            <td>{{ $position->boss ? $position->boss->name : 'Нет начальника' }}</td>
                            <td>
                                <div class="btn-group btn-group-xs">
                                    <div class="btn btn-success create_employee">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <i class="glyphicon glyphicon-user"></i>
                                    </div>
                                    <div class="btn btn-info view_position_employee">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                        <i class="glyphicon glyphicon-user"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group btn-group-xs">
                                    <div class="btn btn-success create_position">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </div>
                                    <div class="btn btn-warning edit_position">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </div>
                                    <div class="btn btn-danger delete_position">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right btn btn-primary create_employee">
                <i class="glyphicon glyphicon-plus"></i>
                Создать сотрудника
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <table id="employees_list" class="table table-condensed">

            </table>
        </div>
    </div>
@endsection

@push('modals')
    <div class="modal fade" id="create_position_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="create_position_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title text-center">Создать должность</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group input-group">
                        <input type="text" class="form-control" name="name" required>
                        <span class="input-group-addon">
                            Название
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="parent_position_id" required>

                        </select>
                        <span class="input-group-addon">
                            Родитель
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="boss_id" required>

                        </select>
                        <span class="input-group-addon">
                            Начальник
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="edit_position_modal" tabindex="-1" role="dialog" aria-hidden="true" data-id="">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="edit_position_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title text-center">Редактировать должность</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group input-group">
                        <input type="text" class="form-control" name="name" required>
                        <span class="input-group-addon">
                            Название
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="parent_position_id" required>

                        </select>
                        <span class="input-group-addon">
                            Родитель
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="boss_id" required>

                        </select>
                        <span class="input-group-addon">
                            Начальник
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="create_employee_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="create_employee_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title text-center">Создать сотрудника</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <label for="upload1">
                                <img src="" alt="Upload avatar" width="200">
                                <input type="file"  accept="image/*" name="avatar" id="upload1" style="display:none">
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group input-group">
                        <input type="text" class="form-control" name="name" required>
                        <span class="input-group-addon">
                            ФИО
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <input type="date" max="{{ date('Y-m-d') }}" class="form-control" name="employment_date" required>
                        <span class="input-group-addon">
                            Работает с
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <input type="number" step="1" min="1" class="form-control" name="salary" required>
                        <span class="input-group-addon">
                            Зарплата
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="position_id" required>

                        </select>
                        <span class="input-group-addon">
                            Должность
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="edit_employee_modal" tabindex="-1" role="dialog" aria-hidden="true" data-id="">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="edit_employee_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title text-center">Редактировать сотрудника</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <label for="upload2">
                                <img src="" alt="Upload avatar" width="200">
                                <input type="file"  accept="image/*" name="avatar" id="upload2" style="display:none">
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group input-group">
                        <input type="text" class="form-control" name="name" required>
                        <span class="input-group-addon">
                            ФИО
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <input type="date" max="{{ date('Y-m-d') }}" class="form-control" name="employment_date" required>
                        <span class="input-group-addon">
                            Работает с
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <input type="number" step="1" min="1" class="form-control" name="salary" required>
                        <span class="input-group-addon">
                            Зарплата
                        </span>
                    </div>
                    <div class="form-group input-group">
                        <select class="selectpicker form-control" data-live-search="true" name="position_id" required>

                        </select>
                        <span class="input-group-addon">
                            Должность
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endpush

@push('styles')
    @css('TreeGrid')
    @css('DataTables')
@endpush

@push('scripts')
    @js('TreeGrid')
    @js('DataTables')
    @js('preview_img')
    @js('admin/employees/list')
@endpush