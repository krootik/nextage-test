<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>
            @if(isset($title) && !empty($title))
                {{ $title }} @ Nextpage
            @else
                Nextpage test
            @endif
        </title>

        <!-- Bootstrap -->
        @css('Bootstrap')

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @css('index')
        @stack('styles')
    </head>
    <body>
        @yield('header')

        @yield('main')

        <footer class="footer">
            <div class="container">
                @yield('footer')
            </div>
        </footer>

        @stack('modals')

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        @js('jQuery')
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        @js('Bootstrap')
        <script>
            let URLS = {!! json_encode($urls) !!}
        </script>
        @js('index')
        @stack('scripts')
    </body>
</html>