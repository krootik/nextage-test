@extends('front.base-layout')

@section('content')
    <table id="employees_list" class="table table-condensed">

    </table>
@endsection

@push('styles')
    @css('DataTables')
@endpush

@push('scripts')
    @js('DataTables')
    @js('front/employees/list')
@endpush