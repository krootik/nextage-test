<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 21.02.2020
 * Time: 12:42
 */

return [
    'js' => static function ($scriptName) {
        $scriptName = trim($scriptName, '\'"');
        switch ($scriptName) {
            case 'jQuery':
                return '<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
                        <script>
                            $.ajaxSetup({
                                beforeSend: function (xhr, settings)
                                {
                                    if (settings.url.match(new RegExp("^(\/\w|' . config('app.url') . ')", "i"))) {
                                        xhr.setRequestHeader("X-CSRF-TOKEN", "' . csrf_token() . '");
                                    }
                                },
                            });
                        </script>';
            case 'Bootstrap':
                return '
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
                    ';
            case 'DataTables':
                return '<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
                        <script>
                            $.extend(true, $.fn.dataTable.defaults, {
                                language: {
                                    url: "//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json"
                                }
                            } );
                        </script>';
                case 'TreeGrid';
                    return '<script src="/libs/TreeGrid/dist/js/jquery.treegrid.min.js"></script>';
        }

        if (!preg_match('/^(https?|\/\/)/i', $scriptName)) {
            $scriptName = sprintf('/js/%s.js', ltrim($scriptName, '/'));
            $scriptName = asset($scriptName);
            if (config('app.env') !== 'production') {
                $scriptName .= '?date=' . time();
            }
        }
        return '<script src="' . $scriptName . '"></script>';
    },
    'css' => static function($stylesheetName) {
        $stylesheetName = trim($stylesheetName, '\'"');

        switch ($stylesheetName) {
            case 'Bootstrap':
                return '
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
                    ';
            case 'DataTables':
                $stylesheetName = '//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css';
                break;
            case 'TreeGrid':
                return '<link type="text/css" rel="stylesheet" href="/libs/TreeGrid/dist/css/jquery.treegrid.css">';
        }

        if (!preg_match('/^(https?|\/\/)/i', $stylesheetName)) {
            $stylesheetName = sprintf('/css/%s.css', ltrim($stylesheetName, '/'));
            $stylesheetName = asset($stylesheetName);
            if (config('app.env') !== 'production') {
                $stylesheetName .= '?date=' . time();
            }
        }
        return '<link type="text/css" rel="stylesheet" href="' . $stylesheetName . '">';
    },
];