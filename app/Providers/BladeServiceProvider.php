<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 21.02.2020
 * Time: 12:35
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() : void
    {
        app('view');
        $bladeDirectives = require base_path() . '/_blade_directives.php';
        collect($bladeDirectives)->each(static function($directiveProcessing, $directiveName) {
            app('blade.compiler')->directive($directiveName, $directiveProcessing);
        });
    }
}