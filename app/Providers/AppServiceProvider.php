<?php

namespace App\Providers;

use App\Repositories\EmployeeRepo;
use App\Repositories\PositionRepo;
use App\Services\EmployeePositionService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EmployeeRepo::class, static function() {
            return new EmployeeRepo();
        });
        $this->app->singleton(PositionRepo::class, static function() {
            return new PositionRepo();
        });

        $this->app->singleton(EmployeePositionService::class, static function() {
            return new EmployeePositionService(resolve(EmployeeRepo::class), resolve(PositionRepo::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
