<?php

namespace App\Models;

use App\Models\File\StorageFile;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Employee
 *
 * @package App\Models
 *
 * @property int                   id
 * @property string                name
 * @property \Carbon\Carbon        employment_date
 * @property float                 salary
 * @property int                   avatar_id
 * @property int                   position_id
 * @property \Carbon\Carbon        created_at
 * @property \Carbon\Carbon        updated_at
 * @property-read StorageFile|null avatar
 * @property-read Position         position
 * @property-read Employee|null    boss
 */
class Employee extends BaseModel
{
    protected $fillable = [
        'name',
        'employment_date',
        'salary',
        'avatar_id',
        'position_id',
    ];

    protected $casts = [
        'salary'      => 'float',
        'avatar_id'   => 'int',
        'position_id' => 'int',
    ];

    protected $dates = [
        'employment_date',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::deleting(static function(Employee $model) {
            $model->avatar_id && $model->avatar->delete();
        });
    }

    public function avatar() : HasOne
    {
        return $this->hasOne(StorageFile::class, 'id', 'avatar_id');
    }

    public function position() : HasOne
    {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    public function boss() : BelongsTo
    {
        return $this->position->boss();
    }
}
