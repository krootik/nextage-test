<?php

namespace App\Models\File;

use App\Models\BaseModel;
use App\Observers\StorageFileObserver;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

/**
 * Class StorageFile
 *
 * @package App\Models\File
 *
 * @property int         id
 * @property string      path
 * @property string      name
 * @property string      hash
 * @property Carbon      created_at
 * @property Carbon      updated_at
 * @property-read string url
 * @property-read string pathname
 */
class StorageFile extends BaseModel
{
    public const UPLOADS_PATH = 'public/uploads/';

    protected $fillable = [
        'path',
        'name',
        'hash'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::deleted(static function(StorageFile $file) {
            Storage::delete($file->pathname);
        });
    }

    public function getUrlAttribute(?string $path = null) : string
    {
        if (isset($path) && empty($path)) {
            return '';
        }
        return Storage::url($path ?: $this->pathname);
    }

    public function getPathnameAttribute() : string {
        return $this->getOriginal('path') . $this->getOriginal('name');
    }

    /**
     * @param string $extension
     *
     * @return string
     */
    public static function uniqueName(string $extension) : string
    {
        do {
            $uniqueName = sprintf('%s.%s', Str::slug(uniqid('', true), ''), $extension);
        } while (Storage::exists($uniqueName));

        return $uniqueName;
    }
}
