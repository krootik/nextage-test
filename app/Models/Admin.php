<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

/**
 * Class Admin
 *
 * @package App\Models
 *
 * @property string login
 * @property string password
 * @property string remember_token
 */
class Admin extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public $fillable = [
        'login',
        'password',
        'remember_token',
    ];

    public $hidden = [
        'password',
        'remember_token'
    ];

    public $timestamps = false;
}
