<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * Class Position
 *
 * @package App\Models
 *
 * @property int             id
 * @property string          name
 * @property int             boss_id
 * @property int             parent_position_id
 * @property-read Employee   boss
 * @property-read Position   parent
 * @property-read Collection subordinate
 */
class Position extends BaseModel
{
    protected $fillable = [
        'name',
        'boss_id',
        'parent_position_id',
    ];

    protected $casts = [
        'boss_id'            => 'int',
        'parent_position_id' => 'int',
    ];

    public $timestamps = false;

    public function parent() : HasOne
    {
        return $this->hasOne(Position::class, 'id', 'parent_position_id');
    }

    public function employees() : HasMany
    {
        return $this->hasMany(Employee::class);
    }

    public function boss() : BelongsTo
    {
        return $this->belongsTo(Employee::class, 'boss_id');
    }

    public function subordinate() : HasMany
    {
        return $this->hasMany(Employee::class, 'position_id');
    }
}
