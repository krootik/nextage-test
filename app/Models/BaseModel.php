<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 15.02.2020
 * Time: 16:49
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public static function getTableName() : string
    {
        $model = new static();
        return $model->getTable();
    }
}