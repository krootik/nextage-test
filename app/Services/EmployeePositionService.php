<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 12:04
 */

namespace App\Services;

use App\Repositories\EmployeeRepo;
use App\Repositories\PositionRepo;
use Illuminate\Support\Str;

class EmployeePositionService
{
    protected $employeeRepo;
    protected $positionRepo;

    public function __construct(EmployeeRepo $employeeRepo, PositionRepo $positionRepo)
    {
        $this->employeeRepo = $employeeRepo;
        $this->positionRepo = $positionRepo;
    }

    public function employees() : EmployeeRepo
    {
        return $this->employeeRepo;
    }

    public function positions() : PositionRepo
    {
        return $this->positionRepo;
    }
}