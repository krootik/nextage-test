<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 14:05
 */

namespace App\Contracts\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;

interface WithRequestContract
{
    public function throwNotValid(FormRequest $request) : void;
}