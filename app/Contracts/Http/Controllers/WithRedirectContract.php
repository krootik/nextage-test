<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 20.02.2020
 * Time: 12:30
 */

namespace App\Contracts\Http\Controllers;

interface WithRedirectContract
{
    public function urlHome() : string;

    public function urlLogin() : string;

    public function urlSuccess() : string;

    public function urlFailed() : string;
}