<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 14:06
 */

namespace App\Contracts\Http\Controllers;

use Illuminate\Http\Response;

interface WithResponseContract
{
    public function success(array $data = [], int $code = Response::HTTP_OK);

    public function error(array $errors = [], int $code = Response::HTTP_BAD_REQUEST);

    public function response(string $message = 'success', array $data = [], array $errors = [], int $code = Response::HTTP_OK);
}