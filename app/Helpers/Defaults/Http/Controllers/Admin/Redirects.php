<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 20.02.2020
 * Time: 12:33
 */

namespace App\Helpers\Defaults\Http\Controllers\Admin;

trait Redirects
{
    public function urlHome() : string
    {
        return route('admin-index');
    }

    public function urlLogin() : string
    {
        return route('admin-login');
    }

    public function urlSuccess() : string
    {
        return $this->urlHome();
    }

    public function urlFailed() : string
    {
        return $this->urlLogin();
    }
}