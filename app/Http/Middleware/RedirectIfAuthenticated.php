<?php

namespace App\Http\Middleware;

use App\Contracts\Http\Controllers\WithRedirectContract;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $redirectController = $request->route()->controller;
            if ($redirectController instanceof WithRedirectContract) {
                return redirect($redirectController->urlSuccess());
            }
            return redirect(route('front-index'));
        }

        return $next($request);
    }
}
