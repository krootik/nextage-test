<?php

namespace App\Http\Middleware;

use App\Contracts\Http\Controllers\WithRedirectContract;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    public function handle($request, \Closure $next, ...$guards)
    {
        try {
            return parent::handle($request, $next, $guards);
        } catch (AuthenticationException $e) {
            if ($request->expectsJson()) {
                throw $e;
            }
            $redirectController = $request->route()->controller;
            if ($redirectController instanceof WithRedirectContract) {
                return redirect($redirectController->urlLogin());
            }

            return redirect(route('front-index'));
        }
    }
}
