<?php

namespace App\Http\Requests\Api;

use App\Models\Position;

class UpdatePosition extends CreatePosition
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'id' => sprintf('required|integer|exists:%s', Position::getTableName()),
            ],
            parent::rules()
        );
    }
}
