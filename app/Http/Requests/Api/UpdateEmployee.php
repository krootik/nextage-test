<?php

namespace App\Http\Requests\Api;

use App\Models\Employee;

class UpdateEmployee extends CreateEmployee
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'id' => sprintf('required|integer|exists:%s', Employee::getTableName()),
            ],
            parent::rules()
        );
    }
}
