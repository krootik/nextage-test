<?php

namespace App\Http\Requests\Api;

use App\Models\Position;
use Illuminate\Foundation\Http\FormRequest;

class CreateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'employment_date' => 'required|date_format:Y-m-d|before:today',
            'salary' => 'required|numeric',
            'avatar' => 'image',
            'position_id' => sprintf('required|integer|exists:%s,id', Position::getTableName()),
        ];
    }
}
