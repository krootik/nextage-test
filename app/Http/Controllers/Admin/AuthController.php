<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 20.02.2020
 * Time: 12:58
 */

namespace App\Http\Controllers\Admin;

use App\Contracts\Http\Controllers\WithRedirectContract;
use App\Exceptions\Http\HttpInvalidEx;
use App\Helpers\Defaults\Http\Controllers\Admin\Redirects as AdminRedirectsDefaults;
use App\Http\Controllers\BaseWebController;
use App\Http\Requests\Admin\Login;
use App\Models\Admin;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthController extends BaseWebController implements WithRedirectContract
{
    use AdminRedirectsDefaults;

    public function __construct()
    {
        \Auth::setDefaultDriver('admin');

        $this->urls = [
            'front' => route('front-index'),
            'login' => route('admin-login'),
            'doLogin' => route('admin-login-action'),
        ];
    }

    public function login()
    {
        if (\Auth::check()) {
            return redirect($this->urlHome());
        }

        return $this->response('Вход в админку', ['admin.auth.login']);
    }

    public function doLogin(Login $request) : RedirectResponse
    {
        try {
            $login = $request->get('login');
            $pass = $request->get('password');
            $remember = $request->get('remember');

            /* @var $admin Admin */
            $admin = Admin::query()->where('login', $login)->first();

            if (!\Hash::check($pass, $admin->getAuthPassword())) {
                throw new HttpInvalidEx(['password' => 'Password is invalid']);
            }

            if (\Hash::needsRehash($admin->getAuthPassword())) {
                $admin->password = \Hash::make($pass);
                $admin->save();
            }

            \Auth::login($admin, !empty($remember));

            return redirect($this->urlSuccess());
        } catch (HttpInvalidEx $e) {
            return redirect($this->urlFailed());
        }
    }

    public function doLogout() : RedirectResponse
    {
        if (\Auth::check()) {
            \Auth::logout();
        }

        return redirect(route('front-index'));
    }
}