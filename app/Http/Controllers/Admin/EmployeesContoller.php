<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Http\Controllers\WithRedirectContract;
use App\Helpers\Defaults\Http\Controllers\Admin\Redirects as AdminRedirectsDefaults;
use App\Http\Controllers\BaseWebController;
use App\Services\EmployeePositionService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeesContoller extends BaseWebController implements WithRedirectContract
{
    use AdminRedirectsDefaults;

    protected $empPosService;

    public function __construct(EmployeePositionService $empPosService)
    {
        $this->empPosService = $empPosService;

        \Auth::setDefaultDriver('admin');

        $this->urls = [
            'front' => route('front-index'),

            'index' => route('admin-index'),
            'employees_list' => route('employee-list'),
            'positions_list' => route('position-list'),
            'employee_get' => route('employee-single', ['id' => 0]),
            'position_get' => route('position-single', ['id' => 0]),
            'employee_create' => route('employee-create'),
            'position_create' => route('position-create'),
            'employee_update' => route('employee-update', ['id' => 0]),
            'position_update' => route('position-update', ['id' => 0]),
            'employee_delete' => route('employee-delete', ['id' => 0]),
            'position_delete' => route('position-delete', ['id' => 0]),

            'doLogout' => route('admin-logout-action'),
        ];
    }

    public function index() : Response
    {
        $this->urls['current'] = $this->urls['index'];

        $positions = $this->empPosService->positions()->getCollection(['parent_position_id' => null],['boss'],-1);

        return $this->response('Список сотрудников', ['admin.employees.list', 'positions' => $positions]);
    }

    public function employee() : Response
    {
        return $this->response('List of employees', ['admin.employees.list']);
    }
}
