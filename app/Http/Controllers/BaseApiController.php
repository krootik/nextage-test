<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 14:00
 */

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

abstract class BaseApiController extends BaseController
{
    /**
     * @param string $message
     * @param array $data
     * @param array $errors
     * @param int   $code
     *
     * @return JsonResponse
     */
    public function response(string $message = 'success', array $data = [], array $errors = [], int $code = Response::HTTP_OK) : JsonResponse
    {
        return new JsonResponse(array_merge(['message' => $message], empty($errors) ? $data : ['errors' => $errors]), $code);
    }
}