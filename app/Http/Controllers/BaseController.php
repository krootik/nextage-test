<?php

namespace App\Http\Controllers;

use App\Contracts\Http\Controllers\WithRequestContract;
use App\Contracts\Http\Controllers\WithResponseContract;
use App\Exceptions\Http\HttpInvalidEx;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

abstract class BaseController extends Controller implements WithRequestContract, WithResponseContract
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public const PER_PAGE = 10;

    public function success(array $data = [], int $code = Response::HTTP_OK)
    {
        return $this->response('success', $data, [], $code);
    }

    public function error(array $errors = [], int $code = Response::HTTP_BAD_REQUEST)
    {
        return $this->response('error', [], $errors, $code);
    }

    public function throwNotValid(FormRequest $request) : void
    {
        try {
            $request->validated();
        } catch (ValidationException $e) {
            throw new HttpInvalidEx($e->errors());
        }
    }

    protected function getLimitOffsetByRequest(Request $request) : array
    {
        return [$request->get('length', self::PER_PAGE), $request->get('start')];
//        $limit = (int) $request->get('per_page', self::PER_PAGE);
//        $limit < 2 && $limit = self::PER_PAGE;
//        $page = (int) $request->get('page', 1) -1;
//        $page < 0 && $page = 0;
//        return [$limit, $page * $limit];
    }

    protected function getSortByRequest(Request $request) : array
    {
        return (array) $request->get('sort', []);
    }
}
