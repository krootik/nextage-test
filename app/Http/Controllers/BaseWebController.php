<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 14:20
 */

namespace App\Http\Controllers;

use Illuminate\Http\Response;

abstract class BaseWebController extends BaseController
{
    protected $urls = [];

    public function response(string $title = 'Nextpage test', array $data = [], array $errors = [], int $code = Response::HTTP_OK) : Response
    {
        $view = array_shift($data);
        return \Response::view($view, ['title' => $title, 'data' => $data, 'errors' => $errors, 'urls' => $this->urls], $code);
    }
}