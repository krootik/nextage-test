<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\Http\HttpInvalidEx;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\Api\CreateEmployee;
use App\Http\Requests\Api\CreatePosition;
use App\Http\Requests\Api\DeleteEmployee;
use App\Http\Requests\Api\DeletePosition;
use App\Http\Requests\Api\UpdateEmployee;
use App\Http\Requests\Api\UpdatePosition;
use App\Models\Employee;
use App\Models\File\StorageFile;
use App\Services\EmployeePositionService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeesContoller extends BaseApiController
{
    protected $empPosService;

    public function __construct(EmployeePositionService $empPosService)
    {
        $this->empPosService = $empPosService;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function getEmployees(Request $request) : JsonResponse
    {
        $errors = [];
        try {
            $searchConditions = [];
            $filtersConditions = [];

            if ($searh = $request->get('search', '')) {
                if (is_integer($searh)) {
                    $searchConditions[] = ['id', '=', $searh, 'or'];
                } elseif (is_float($searh)) {
                    $searchConditions[] = ['salary', '>=', (int) $searh, 'or'];
                }
                try {
                    $asDate = Carbon::createFromFormat('Y-m-d', $searh);
                    $searchConditions[] = ['employment_date', '>=', $asDate, 'or'];
                } catch (\Throwable $e) {
                }
                $searchConditions[] = ['name', 'like', sprintf('%s%%', (string) $searh), 'or'];
            }

            $fieldsFilters = $request->only($this->empPosService->employees()->model->getFillable());
            if (!empty($fieldsFilters)) {
                $filtersConditions = array_filter($fieldsFilters);
                foreach ($fieldsFilters as $field => $value) {
                    $filtersConditions[$field] = $value ?: null;
                }
            }

            $sortFields = $this->getSortByRequest($request);
            if (isset($sortFields['position'])) {
                $sortFields['position_id'] = $sortFields['position'];
                unset($sortFields['position']);
            }

            $employees = $this->empPosService
                ->employees()
                ->fillBuilder(
                    $this->empPosService->employees()->getBuilder()->where($filtersConditions),
                    $searchConditions,
                    ['position', 'avatar'],
                    array_filter($this->getLimitOffsetByRequest($request)),
                    $sortFields
                )
                ->get()
                ->map(static function(Employee $employee) {
                    $employee->position_name = $employee->position ? $employee->position->name : null;
                    $employee->ava_url = $employee->avatar ? asset($employee->avatar->url) : null;
                    $employee->works_from = $employee->employment_date->format('Y-m-d');

                    return $employee;
                });

            return $this->success([
                'draw' => (int) $request->get('draw', 1),
                'recordsTotal' => $this->empPosService->employees()->count($filtersConditions),
                'recordsFiltered' => $this->empPosService
                    ->employees()
                    ->fillBuilder(
                        $this->empPosService->employees()->getBuilder()->where($filtersConditions),
                        $searchConditions
                    )
                    ->count(),
                'data' => $employees->toArray()
            ]);
        } catch (\Throwable $e) {
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function getEmployee(Request $request) : JsonResponse
    {
        $errors = [];
        try {
            /* @var $employee Employee */
            $employee = $this->empPosService
                ->employees()
                ->getOne(
                    $request->get('id'),
                    ['position', 'avatar'],
                    $this->getSortByRequest($request)
                );

            $employee->ava_url = $employee->avatar ? asset($employee->avatar->url) : null;
            $employee->works_from = $employee->employment_date->format('Y-m-d');
            return $this->success($employee->toArray());
        } catch (\Throwable $e) {
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param CreateEmployee $request
     *
     * @return JsonResponse|Response
     */
    public function createEmployee(CreateEmployee $request) : JsonResponse
    {
        $errors = [];
        $avatarPath = null;

        try {
            $this->throwNotValid($request);

            $empRepo = $this->empPosService->employees();

            \DB::beginTransaction();
            if ($avatar = $request->file('avatar')) {
                $name = StorageFile::uniqueName($avatar->guessClientExtension());
                if (!($avatarPath = \Storage::putFileAs(StorageFile::UPLOADS_PATH, $avatar, $name))) {
                    throw new \Exception('Unable to save file');
                }
                $request->merge([
                    'avatar_id' => StorageFile::create([
                                        'name' => $name,
                                        'path' => StorageFile::UPLOADS_PATH,
                                        'hash' => $avatar->hashName()
                                    ])->getKey()
                ]);
            }
            $resp = $empRepo->create($request->only($empRepo->model->getFillable()))->toArray();
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();

            if ($avatarPath) {
                \Storage::delete($avatarPath);
            }

            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateEmployee $request
     *
     * @return JsonResponse|Response
     */
    public function updateEmployee(UpdateEmployee $request) : JsonResponse
    {
        $errors = [];
        $avatarPath = null;
        try {
            $this->throwNotValid($request);

            $empRepo = $this->empPosService->employees();

            \DB::beginTransaction();
            /* @var $employee Employee */
            $employee = $empRepo->getOne($request->get('id'), 'avatar');
            if ($avatar = $request->file('avatar')) {
                $name = StorageFile::uniqueName($avatar->guessClientExtension());
                if (!($avatarPath = \Storage::putFileAs(StorageFile::UPLOADS_PATH, $avatar, $name))) {
                    throw new \Exception('Unable to save file');
                }
                $request->merge([
                    'avatar_id' => StorageFile::create([
                        'name' => $name,
                        'path' => StorageFile::UPLOADS_PATH,
                        'hash' => $avatar->hashName()
                    ])->getKey()
                ]);
                $employee->avatar && $employee->avatar->delete();
            }

            $resp = $empRepo->update(
                $employee,
                $request->only($empRepo->model->getFillable())
            )->toArray();
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();
            if ($avatarPath) {
                \Storage::delete($avatarPath);
            }
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param DeleteEmployee $request
     *
     * @return JsonResponse|Response
     */
    public function deleteEmployee(DeleteEmployee $request) : JsonResponse
    {
        $errors = [];
        try {
            $this->throwNotValid($request);

            $empRepo = $this->empPosService->employees();

            \DB::beginTransaction();
            $resp = [
                'deleted' => $empRepo->delete(
                    $empRepo->getOne($request->get('id'))
                )
            ];
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function getPositions(Request $request) : JsonResponse
    {
        $errors = [];
        try {
            $filtersConditions = [];

            $fieldsFilters = $request->only($this->empPosService->positions()->model->getFillable());
            if (!empty($fieldsFilters)) {
                foreach ($fieldsFilters as $field => $value) {
                    $filtersConditions[] = [$field, '=', $value];
                }
            }

            $positions = $this->empPosService
                ->positions()
                ->getCollection(
                    $filtersConditions,
                    ['boss'],
                    array_filter($this->getLimitOffsetByRequest($request)),
                    $this->getSortByRequest($request)
                );

            return $this->success(['positions' => $positions->values()->toArray()]);
        } catch (\Throwable $e) {
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function getPosition(Request $request) : JsonResponse
    {
        $errors = [];
        try {
            $employee = $this->empPosService
                ->positions()
                ->getOne(
                    $request->get('id'),
                    [],
                    $this->getSortByRequest($request)
                );

            return $this->success($employee->toArray());
        } catch (\Throwable $e) {
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param CreatePosition $request
     *
     * @return JsonResponse|Response
     */
    public function createPosition(CreatePosition $request) : JsonResponse
    {
        $errors = [];
        try {
            $this->throwNotValid($request);

            $posRepo = $this->empPosService->positions();

            \DB::beginTransaction();
            $resp = $posRepo->create($request->only($posRepo->model->getFillable()))->toArray();
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdatePosition $request
     *
     * @return JsonResponse|Response
     */
    public function updatePosition(UpdatePosition $request) : JsonResponse
    {
        $errors = [];
        try {
            $this->throwNotValid($request);

            $posRepo = $this->empPosService->positions();

            \DB::beginTransaction();
            $resp = $posRepo->update(
                $posRepo->getOne($request->get('id')),
                $request->only($posRepo->model->getFillable())
            )->toArray();
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param DeletePosition $request
     *
     * @return JsonResponse|Response
     */
    public function deletePosition(DeletePosition $request) : JsonResponse
    {
        $errors = [];
        try {
            $this->throwNotValid($request);

            \DB::beginTransaction();
            $posRepo = $this->empPosService->positions();
            $resp = [
                'deleted' => $posRepo->delete(
                    $posRepo->getOne($request->get('id'))
                )
            ];
            \DB::commit();

            return $this->success($resp);
        } catch (HttpInvalidEx $e) {
            return $this->error($e->getFieldsInvalid());
        } catch (\Throwable $e) {
            \DB::rollBack();
            return $this->response($e->getMessage(), [], $errors, $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
