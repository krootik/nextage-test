<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseWebController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeesContoller extends BaseWebController
{
    public function __construct()
    {
        $this->urls = [
            'admin' => route('admin-index'),

            'index' => route('front-index'),
            'list' => route('employee-list'),
        ];
    }

    public function index() : Response
    {
        $this->urls['current'] = $this->urls['index'];

        return $this->response('Список сотрудников', ['front.employees.list']);
    }
}
