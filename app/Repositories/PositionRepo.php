<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 15.02.2020
 * Time: 16:41
 */

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\Position;

class PositionRepo extends BaseRepo
{
    public function __construct()
    {
        $this->model = new Position();
    }

    /**
     * @param BaseModel|Position $model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(BaseModel $model) : bool
    {
        /* @var $empRepo EmployeeRepo */
        $empRepo = resolve(EmployeeRepo::class);
        $empRepo
            ->fillBuilder(
                $empRepo->getBuilder(),
                ['position_id' => $model->getKey()]
            )
            ->update(['position_id' => $model->parent_position_id]);
        $this
            ->fillBuilder(
                $this->getBuilder(),
                ['parent_position_id' => $model->getKey()]
            )
            ->update(['parent_position_id' => $model->parent_position_id]);

        return parent::delete($model);
    }
}