<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 15.02.2020
 * Time: 16:44
 */

namespace App\Repositories;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

abstract class BaseRepo
{
    /* @var $model BaseModel */
    public $model;

    abstract function __construct();

    /**
     * @param array $fields
     *
     * @return BaseModel
     */
    public function create(array $fields) : BaseModel
    {
        $class = get_class($this->model);

        return $this->update(new $class, $fields);
    }

    /**
     * @param BaseModel $model
     * @param array     $fields
     *
     * @return BaseModel
     */
    public function update(BaseModel $model, array $fields) : BaseModel
    {
        $model->fill($fields);
        $model->save();
        return $model;
    }

    /**
     * @param BaseModel $model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(BaseModel $model) : bool
    {
        return $model->delete();
    }

    /**
     * @param array|int    $conditions
     * @param array|string $relations
     * @param array        $sort
     *
     * @return \Illuminate\Database\Eloquent\Model|BaseModel
     */
    public function getOne(
        $conditions = [],
        $relations = [],
        array $sort = []
    ) : BaseModel
    {
        $builder = $this->fillBuilder(
            $this->getBuilder(),
            is_numeric($conditions) ? [$this->model->getKeyName() => $conditions] : (array) $conditions,
            (array) $relations,
            [1],
            $sort
        );
        return $builder->first();
    }

    /**
     * @param array        $conditions
     * @param array|string $relations
     * @param array|int    $limitOffset
     * @param array        $sort
     *
     * @return Collection
     */
    public function getCollection(
        array $conditions = [],
        $relations = [],
        $limitOffset = 10,
        array $sort = []
    ) : Collection
    {
        $builder = $this->fillBuilder(
            $this->getBuilder(),
            $conditions,
            (array) $relations,
            (array) $limitOffset,
            $sort
        );
        return $builder->get();
    }

    /**
     * @param array        $conditions
     *
     * @return int
     */
    public function count(array $conditions = []) : int {
        $builder = $this->fillBuilder(
            $this->getBuilder(),
            $conditions
        );
        return $builder->count();
    }

    /**
     * @return Builder
     */
    public function getBuilder() : Builder
    {
        return $this->model->newQuery();
    }

    /**
     * @param Builder $builder
     * @param array   $conditions
     * @param array   $relations
     * @param array   $limitOffset
     * @param array   $sort
     *
     * @return Builder
     */
    public function fillBuilder(Builder $builder, array $conditions = [], array $relations = [], array $limitOffset = [10], array $sort = []) : Builder
    {
        $builder
            ->where($conditions)
            ->with($relations);
        $limit = (int) array_shift($limitOffset);
        $offset = (int) array_shift($limitOffset);
        if ($limit > 0) {
            $builder->limit($limit);
        }
        if ($offset) {
            $builder->offset($offset);
        }
        foreach ($sort as $i => $field) {
            $dir = 'desc';
            if (is_string($i)) {
                $dir = $field;
                $field = $i;
            }
            $builder->orderBy($field, $dir);
        }
        return $builder;
    }

    /**
     * @param BaseModel $model
     */
    public function setModel($model) : void
    {
        $this->model = $model;
    }
}