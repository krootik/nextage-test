<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 15.02.2020
 * Time: 16:41
 */

namespace App\Repositories;

use App\Models\Employee;

class EmployeeRepo extends BaseRepo
{
    public function __construct()
    {
        $this->model = new Employee();
    }
}