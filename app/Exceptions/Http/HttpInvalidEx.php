<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 16.02.2020
 * Time: 14:10
 */

namespace App\Exceptions\Http;

use Illuminate\Http\Response;
use Throwable;

class HttpInvalidEx extends \Exception
{
    protected $fieldsInvalid;

    public function __construct(array $fieldsInvalid, Throwable $previous = null)
    {
        $this->fieldsInvalid = $fieldsInvalid;
        parent::__construct('Invalid input', Response::HTTP_BAD_REQUEST, $previous);
    }

    /**
     * @return array
     */
    public function getFieldsInvalid() : array
    {
        return $this->fieldsInvalid;
    }
}